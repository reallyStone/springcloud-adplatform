package com.wwmxd.service.impl;

import com.wwmxd.entity.Element;
import com.wwmxd.dao.ElementDao;
import com.wwmxd.service.ElementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
@Service
public class ElementServiceImpl extends ServiceImpl<ElementDao, Element> implements ElementService  {
	
}
