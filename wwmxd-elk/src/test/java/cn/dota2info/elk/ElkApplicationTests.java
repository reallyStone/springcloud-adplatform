package cn.dota2info.elk;


import cn.dota2info.elk.controller.BookSearchController;
import cn.dota2info.elk.dao.ElkDocDao;
import cn.dota2info.elk.entity.BaseSearchParam;
import cn.dota2info.elk.entity.Book;
import cn.dota2info.elk.utils.MapUtils;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ElkApplicationTests {
	@Autowired
	private RestHighLevelClient client;
	@Autowired
	private ElkDocDao elkDao;

	@Autowired
	private BookSearchController searchController;


	Logger logger= LoggerFactory.getLogger(ElkApplicationTests.class);
	@Test
	public void contextLoads() {
		SearchRequest searchRequest = new SearchRequest();
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(QueryBuilders.matchAllQuery());
		searchSourceBuilder.from(0);
		searchSourceBuilder.size(5);
		searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
		searchRequest.types("cssp");
		searchRequest.source(searchSourceBuilder);
		try {
			SearchResponse searchResponse=client.search(searchRequest);
			SearchHits hits = searchResponse.getHits();
			long totalHits = hits.getTotalHits();
			float maxScore = hits.getMaxScore();
			SearchHit[] searchHits = hits.getHits();
			for (SearchHit hit : searchHits) {
				// do something with the SearchHit
				String sourceAsString = hit.getSourceAsString();
				Map<String, Object> sourceAsMap = hit.getSourceAsMap();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testExists(){
		logger.info("exists:"+elkDao.existsObject("book","doc","1"));
	}

	@Test
	public void getAll() throws Exception {
		MultiGetRequest request = new MultiGetRequest();
		try {
			SearchRequest searchRequest = new SearchRequest("book");
			searchRequest.types("doc");
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(QueryBuilders.matchAllQuery());
			searchRequest.source(searchSourceBuilder);
			SearchResponse searchResponse = client.search(searchRequest);
			SearchHits hits = searchResponse.getHits();
			SearchHit[] searchHits = hits.getHits();
			for (SearchHit hit : searchHits) {
				Map<String, Object> sourceAsMap = hit.getSourceAsMap();
				Book book= (Book) MapUtils.map2Object(sourceAsMap,Book.class,hit.getId());
				logger.info("book:"+book);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 测试search Api
	 * @throws Exception
	 */
	@Test
	public void search() throws Exception {

	}
}
